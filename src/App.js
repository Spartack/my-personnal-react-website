import React from "react";
import "./App.css";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import Menu from "./components/Menu.js";
import Home from "./components/Home.js";
import About from "./components/About.js";

function App() {
  return (
    <Router>
      <div className="App">
        <div className="body-wrapper">
          <Router>
            <Switch>
              <Route exact path="/" component={Home} />
            </Switch>
            <Switch>
              <Route path="/a-propos" component={About} />
            </Switch>
            <Menu />
          </Router>
        </div>
      </div>
    </Router>
  );
}

export default App;
