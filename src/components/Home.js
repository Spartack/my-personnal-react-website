import React from "react";
import "./home.css";

const Home = () => {
  return (
    <div className="home">
      <h2>Nicolas MARET</h2>
      <p> Développeur Web Fullstack</p>
      <div className="logos">
        <i class="fab fa-react react"></i>
        <i class="fab fa-node node-js"></i>
        <i class="fab fa-node-js js"></i>
      </div>
    </div>
  );
};

export default Home;
