import React from "react";
import "./header.css";
import avatar from "../public/avatar.jpg";
import { Link } from "react-router-dom";

const Header = (props) => {
  return (
    <div className="menu__header">
      <img alt="Nicolas développeur" src={avatar}></img>
      <div className="menu__title">
        <h2>Nicolas Maret</h2>
        <p>Développeur Fullstack</p>
      </div>

      <ul className="menu__navigation">
        <li>
          <Link to="/" onClick={props.clickHandler}>
            Home
          </Link>
        </li>
        <li>
          <Link to="/a-propos" onClick={props.clickHandler}>
            A propos
          </Link>
        </li>
        <li onClick={props.clickHandler}>
          <a href="#">CV</a>
        </li>
        <li onClick={props.clickHandler}>
          <a href="#">Portefolio</a>
        </li>
        <li onClick={props.clickHandler}>
          <a href="#">Contact</a>
        </li>
      </ul>
      <ul className="menu__social-medias">
        <li>
          <i class="fab fa-facebook-f"></i>
        </li>
        <li>
          <i class="fab fa-instagram"></i>
        </li>
        <li>
          <i class="fab fa-github"></i>
        </li>
        <li>
          <i class="fab fa-linkedin-in"></i>
        </li>
      </ul>

      <div className="menu__download-btn">Télécharger CV</div>
      <div className="menu__copyright">
        all rights reserved. Made with react
      </div>
    </div>
  );
};

export default Header;
