import React from "react";
import "./about.css";

const About = () => {
  return (
    <div className="about-wrapper">
      <h3>
        A propos de <span>moi</span>
      </h3>

      <div className="row">
        <div className="col-sm-12 col-md-6 about-me">
          <p>
            Proin volutpat mauris ac pellentesque pharetra. Suspendisse congue
            elit vel odio suscipit, sit amet tempor nisl imperdiet. Quisque ex
            justo, faucibus ut mi in, condimentum finibus dolor. Aliquam vitae
            hendrerit dolor, eget imperdiet mauris. Maecenas et ante id ipsum
            condimentum dictum et vel massa. Ut in imperdiet dolor, vel
            consectetur dui.
          </p>
        </div>
        <div className="col-sm-12 col-md-6 infos">
          <div>
            <span>Age </span>28
          </div>
          <div>
            <span>Langues </span>Francais, Anglais
          </div>
          <div>
            <span>Email </span>
            <a href="mailto:maret.nicolas@gmail.com">maret.nicolas@gmail.com</a>
          </div>
          <div>
            <span>Téléphone </span>+33 (0)7 71 67 04 64
          </div>
        </div>
      </div>

      <h3>
        Mes <span>compétences</span>
      </h3>

      <div className="row">
        <div className="col-sm-12 col-md-6 skills-content">
          <i class="fas fa-desktop fa-2x"></i>
          <h4>Développement Web</h4>
          <p>
            Proin volutpat mauris ac pellentesque pharetra. Suspendisse congue
            elit vel odio suscipit, sit amet tempor nisl imperdiet. Quisque ex
            justo, faucibus ut mi in
          </p>
        </div>

        <div className="col-sm-12 col-md-6 skills-content">
          <i class="fas fa-mobile-alt fa-2x"></i>
          <h4>Développement mobile</h4>
          <p>
            Proin volutpat mauris ac pellentesque pharetra. Suspendisse congue
            elit vel odio suscipit, sit amet tempor nisl imperdiet. Quisque ex
            justo, faucibus ut mi in
          </p>
        </div>

        <div className="col-sm-12 col-md-6 skills-content">
          <i class="fas fa-shopping-cart fa-2x"></i>
          <h4>E-Commerce</h4>
          <p>
            Proin volutpat mauris ac pellentesque pharetra. Suspendisse congue
            elit vel odio suscipit, sit amet tempor nisl imperdiet. Quisque ex
            justo, faucibus ut mi in
          </p>
        </div>

        <div className="col-sm-12 col-md-6 skills-content">
          <i class="fas fa-server fa-2x"></i>
          <h4>Back-End</h4>
          <p>
            Proin volutpat mauris ac pellentesque pharetra. Suspendisse congue
            elit vel odio suscipit, sit amet tempor nisl imperdiet. Quisque ex
            justo, faucibus ut mi in
          </p>
        </div>
      </div>
    </div>
  );
};

export default About;
